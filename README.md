# RAF Airman Selection Test #
## 'Work Rate' Segment Emulator ##

This simple game is an emulation of the RAF 'Work Rate' portion of the AST (Aptitude Test). A sequence is given alongside a table. 

The symbols in the table can be used to substitute (by column) the characters to form another string. From the given sequence, many possible and equally correct strings are possible, but only one correct string will be listed in the 4 answers generated. You must select the correct string by pressing 1-4 to select the right answer.

In the real test, you will have 4 minutes to answer 20 questions. This is best distributed as a maximum question time of 12 seconds. For each question, less than 12 seconds should be spent.

### Technical Information ###

This Python 3 program must be ran in a terminal supporting Unicode characters (IDLE should be fine).

Three symbol sets are used: Letters, Shapes and Numbers. The Shapes symbol set is constructed from 4 Unicode shapes.