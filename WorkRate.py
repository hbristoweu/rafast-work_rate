from sys        import stdout
from random     import shuffle,randrange
import numpy as np
import time

BLKCIR = u'\u25CF'  # Black Circle
BLKDIA = u'\u25C6'  # Black Diamond
BLKUTR = u'\u25B2'  # Black Triangle (Up)
BLKSQR = u'\u25a0'  # Black Square

SYMSHP = BLKCIR+BLKDIA+BLKUTR+BLKSQR

SYMLET = 'QWERTYZXCVB'

# Taken from the Python O'Reilly book
class Timer:
    def __init__(self, func=time.perf_counter):
        self.elapsed = 0.0
        self._func = func
        self._start = None

    def start(self):
        if self._start is not None:
            raise RuntimeError('Already started')
        self._start = self._func()

    def stop(self):
        if self._start is None:
            raise RuntimeError('Not started')
        end = self._func()
        self.elapsed += end - self._start
        self._start = None

    def reset(self):
        self.elapsed = 0.0

    @property
    def running(self):
        return self._start is not None

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args):
        self.stop()

def print_help():
  print("\nThis simple game is an emulation of the RAF 'Work Rate' portion of the AST (Aptitude Test). A sequence is given alongside a table. The symbols in the table can be used to substitute (by column) the characters to form another string. From the given sequence, many possible and equally correct strings are possible, but only one correct string will be listed in the 4 answers generated. You must select the correct string by pressing 1-4 to select the right answer.\n")
  print("\nIn the real test, you will have 4 minutes to answer 20 questions. This is best distributed as a maximum question time of 12 seconds. For each question, less than 12 seconds should be spent.\n")
  print("\nPress enter key to start. Press Ctrl+C to stop playing.")
  input('')
  print("\n\n\n")

def get4symbols():
  lst = list(SYMSHP)
  shuffle(lst)
  rtn = ''.join(lst)
  return ''.join(rtn)

def get4letters():
  lst = list(SYMLET)
  shuffle(lst)
  return ''.join(lst[:4])

def get4numbers():
  rtn = ""
  while len(rtn) < 4:
    x = str(randrange(1,9))
    if x not in rtn:
      rtn += x
  return rtn

def getTable():
  sym, let, num = get4symbols(), get4letters(), get4numbers()
  tab = []
  
  for i in range(4):
    tab += [(num[i], let[i], sym[i])]

  return zip(*tab)

def printTable(tab):
  for r in tab:
    print(r)

def nicePrint(row):
  for i in range(len(row)):
    stdout.write(row[i] + '\t')

def ziplist(l):
  return list(zip(*l))
  
def playRound(t):
  tartab = list(t)
  prntab = list(tartab)   # Yes Python really is this stupid
  
  tar = list(''.join(list(tartab)[0]))[:3]
  shuffle(tar)
  print("\nWhich of the following matches this sequence?\n\n\n\t    ", tar, '\n\n\n')

  np.random.shuffle(prntab)
  for r in prntab:
    stdout.write('\t')
    nicePrint(r)
    stdout.write('\n\n')

  # Set of choices
  choices = list()

  # Formulate the right answer
  correct = ""
  correct_n = ""
  flip_seq = ziplist(tartab)
  for i in tar:
    for m in flip_seq:
      if m[0] == i:
        correct_n += m[0]
        correct += m[randrange(1,3)]

  choices.append(correct)

  # Formulate 4 wrong answers
  flip_seq = ziplist(tartab)
  wrong = [""] * 4
  for j in range(len(wrong)):
    wrong_seq = list(tartab[0])
    shuffle(wrong_seq)
    # Salting for harder results
    if j == 0: wrong_seq[0] = correct_n[0]
    if j == 1: wrong_seq[1] = correct_n[1]
    
    while wrong_seq[:3] == tar or wrong_seq[:3] == correct_n:
      shuffle(wrong_seq)
      # Salting for harder results
      if j == 0: wrong_seq[0] = correct_n[0]
      if j == 1: wrong_seq[1] = correct_n[1]

    for i in wrong_seq[:3]:
      for c in flip_seq:
        if i == c[0]:
          wrong[j] += c[randrange(1,3)]

  choices.extend(wrong)
  shuffle(choices)

  # Find which answer is correct after shuffle
  correct_idx = None
  for i in range(len(choices)):
    if choices[i] == correct:
      correct_idx = i+1

  stdout.write(('-'*48) + "\n\n      ")
  for w in choices:
    stdout.write(w + '   ')
  stdout.write("\n\n")

  answer = int(input("\t>"))

  if int(answer) == int(correct_idx):
    print("CORRECT!")
    return True
  else:
    print("INCORRECT! Correct answer: ", correct_idx)
    return False
 
def main():
  try:
    print_help()
    score, games = 0, 0

    t = Timer()
    roundtimer = Timer()
    t.start()
    
    while True:
      roundtimer.start()
      
      print("Game #", games)
      tab = getTable()
      if playRound(tab): score += 1
      games += 1
      
      roundtimer.stop()
      print("Completed in: ", round(roundtimer.elapsed, 2), " / 12.00 seconds.")
      roundtimer.reset()
  except KeyboardInterrupt as e:
    t.stop()
    try:
      roundtimer.stop()
    except RuntimeError:
      pass
    print("\n\nFinal score: ", score, '/', games, " in ", round(t.elapsed, 2), " seconds")

if __name__ == "__main__":
  main()
